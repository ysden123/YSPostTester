# YSPostTester #
Java desktop UI application for testing POST requests.  
YSPostTester intended for tests with POST requests.  
YSPostTester allows
* edit/update HTTP headers
* edit/update POST body
* save/load request configurations

## Downloads - binaries ##
Java 8 is required to execute the application.

Command to execute: java -jar ysposttester...jar

### v 0.0.1 - https://www.dropbox.com/sh/kh4ds69y16zxhm8/AAA3bHzjlNQ0eQiAp5CDZJBRa?dl=0 ###
### v 1.0.1 - https://www.dropbox.com/s/6a2q5j8b05efc4w/YSPostTester_1.0.1.zip?dl=0