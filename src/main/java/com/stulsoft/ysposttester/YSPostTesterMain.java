/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Main application class.
 * 
 * @author Yuriy Stul
 *
 */
public class YSPostTesterMain extends Application {
	private static Logger logger = LogManager.getLogger(YSPostTesterMain.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		logger.debug("Start application.");
		try {
			VBox root = FXMLLoader.load(getClass().getResource("/fxml/mainview.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("YSPostTester (1.0.1)");
			primaryStage.show();
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}

	/**
	 * @param args
	 *            the application command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
		logger.debug("Finish application.");
	}
}
