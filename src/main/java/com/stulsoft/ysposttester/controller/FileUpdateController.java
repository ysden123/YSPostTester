/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.controller;

import java.net.URL;
import java.util.ResourceBundle;
import com.stulsoft.ysposttester.model.RequestFile;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller for header update dialog.
 * 
 * @author Yuriy Stul
 *
 */
public class FileUpdateController {
	private Stage dialogStage;
	private RequestFile requestFile;
	private boolean isOkClicked = false;

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="cancelButton"
	private Button cancelButton; // Value injected by FXMLLoader

	@FXML // fx:id="fileParamName"
	private TextField fileParamName; // Value injected by FXMLLoader

	@FXML // fx:id="fileName"
	private TextField fileName; // Value injected by FXMLLoader

	@FXML // fx:id="okButton"
	private Button okButton; // Value injected by FXMLLoader

	/**
	 * @return the dialogStage
	 */
	public Stage getDialogStage() {
		return dialogStage;
	}

	/**
	 * @param dialogStage
	 *            the dialogStage to set
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * @return the requestFile
	 */
	public RequestFile getRequestFile() {
		return requestFile;
	}

	/**
	 * @param requestFile
	 *            the requestFile to set
	 */
	public void setRequestFile(RequestFile requestFile) {
		this.requestFile = requestFile;

		fileParamName.textProperty().bindBidirectional(requestFile.getFileParamNameProperty());
		fileName.textProperty().bindBidirectional(requestFile.getFileNameProperty());
	}

	/**
	 * @return the isOkClicked
	 */
	public boolean isOkClicked() {
		return isOkClicked;
	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		assert cancelButton != null : "fx:id=\"cancelButton\" was not injected: check your FXML file 'fileupdate.fxml'.";
		assert fileParamName != null : "fx:id=\"fileParamName\" was not injected: check your FXML file 'fileupdate.fxml'.";
		assert fileName != null : "fx:id=\"fileName\" was not injected: check your FXML file 'fileupdate.fxml'.";
		assert okButton != null : "fx:id=\"okButton\" was not injected: check your FXML file 'fileupdate.fxml'.";
	}

	@FXML
	public void onOkButton() {

		String errorMessage = "";
		if (requestFile.getFileName().isEmpty()) {
			errorMessage += " File name is empty.";
		}

		if (errorMessage.isEmpty()) {
			isOkClicked = true;
			dialogStage.close();
		} else {
			Alert alert = new Alert(AlertType.ERROR, errorMessage);
			alert.showAndWait();
		}
	}

	@FXML
	public void onCancelButton() {
		isOkClicked = false;
		dialogStage.close();
	}

}
