/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.stulsoft.ysposttester.model.RequestHeader;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller for header update dialog.
 * 
 * @author Yuriy Stul
 *
 */
public class HeaderUpdateController {
	private Stage dialogStage;
	private RequestHeader requestHeader;
	private boolean isOkClicked = false;

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="cancelButton"
	private Button cancelButton; // Value injected by FXMLLoader

	@FXML // fx:id="headerName"
	private TextField headerName; // Value injected by FXMLLoader

	@FXML // fx:id="headerValue"
	private TextField headerValue; // Value injected by FXMLLoader

	@FXML // fx:id="okButton"
	private Button okButton; // Value injected by FXMLLoader

	/**
	 * @return the dialogStage
	 */
	public Stage getDialogStage() {
		return dialogStage;
	}

	/**
	 * @param dialogStage
	 *            the dialogStage to set
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * @return the requestHeader
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * @param requestHeader
	 *            the requestHeader to set
	 */
	public void setRequestHeader(RequestHeader requestHeader) {
		this.requestHeader = requestHeader;

		headerName.textProperty().bindBidirectional(requestHeader.getHeaderNameProperty());
		headerValue.textProperty().bindBidirectional(requestHeader.getHeaderValueProperty());
	}

	/**
	 * @return the isOkClicked
	 */
	public boolean isOkClicked() {
		return isOkClicked;
	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		assert cancelButton != null : "fx:id=\"cancelButton\" was not injected: check your FXML file 'headerupdate.fxml'.";
		assert headerName != null : "fx:id=\"headerName\" was not injected: check your FXML file 'headerupdate.fxml'.";
		assert headerValue != null : "fx:id=\"headerValue\" was not injected: check your FXML file 'headerupdate.fxml'.";
		assert okButton != null : "fx:id=\"okButton\" was not injected: check your FXML file 'headerupdate.fxml'.";
	}

	@FXML
	public void onOkButton() {

		String errorMessage = "";

		if (requestHeader.getHeaderName().isEmpty()) {
			errorMessage += " Header name is empty.";
		}
		if (requestHeader.getHeaderValue().isEmpty()) {
			errorMessage += " Header value is empty.";
		}

		if (errorMessage.isEmpty()) {
			isOkClicked = true;
			dialogStage.close();
		} else {
			Alert alert = new Alert(AlertType.ERROR, errorMessage);
			alert.showAndWait();
		}
	}

	@FXML
	public void onCancelButton() {
		isOkClicked = false;
		dialogStage.close();
	}

}
