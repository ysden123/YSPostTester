/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stulsoft.ysposttester.data.ConfigDirectory;
import com.stulsoft.ysposttester.data.PostRequestData;
import com.stulsoft.ysposttester.data.RequestConfigData;
import com.stulsoft.ysposttester.data.RequestFileData;
import com.stulsoft.ysposttester.data.RequestHeaderData;
import com.stulsoft.ysposttester.data.RequestParamData;
import com.stulsoft.ysposttester.model.Model;
import com.stulsoft.ysposttester.model.PostRequest;
import com.stulsoft.ysposttester.model.RequestFile;
import com.stulsoft.ysposttester.model.RequestHeader;
import com.stulsoft.ysposttester.model.RequestParam;
import com.stulsoft.ysposttester.net.HttpService;
import com.stulsoft.ysposttester.net.HttpsService;
import com.stulsoft.ysposttester.net.IHttpService;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author Yuriy Stul
 *
 */
public class MainViewController
{
	private Logger logger = LogManager.getLogger(MainViewController.class);

	private Model model;

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="url"
	private TextField url;

	@FXML // fx:id="headerTable"
	private TableView<RequestHeader> headerTable;

	@FXML // fx:id="fileTable"
	private TableView<RequestFile> fileTable;

	@FXML // fx:id="paramTable"
	private TableView<RequestParam> paramTable;

	@FXML // fx:id="addHeaderButton"
	private Button addHeaderButton;

	@FXML // fx:id="editHeaderButton"
	private Button editHeaderButton;

	@FXML // fx:id="deleteHeaderButton"
	private Button deleteHeaderButton;

	@FXML // fx:id="addFileButton"
	private Button addFileButton;

	@FXML // fx:id="editFileButton"
	private Button editFileButton;

	@FXML // fx:id="deleteFileButton"
	private Button deleteFileButton;

	@FXML // fx:id="addParamButton"
	private Button addParamButton;

	@FXML // fx:id="editParamButton"
	private Button editParamButton;

	@FXML // fx:id="deleteParamButton"
	private Button deleteParamButton;

	@FXML
	private TableColumn<RequestHeader, String> headerNameColumn;

	@FXML
	private TableColumn<RequestHeader, String> headerValueColumn;

	@FXML
	private TableColumn<RequestFile, String> fileParamNameColumn;

	@FXML
	private TableColumn<RequestFile, String> fileNameColumn;

	@FXML
	private TableColumn<RequestParam, String> paramNameColumn;

	@FXML
	private TableColumn<RequestParam, String> paramValueColumn;

	@FXML
	private TextArea body;

	@FXML
	private TextArea response;

	@FXML // This method is called by the FXMLLoader when initialization is
			// complete
	private void initialize()
	{
		assert url != null : "fx:id=\"url\" was not injected: check your FXML file 'mainview.fxml'.";
		assert body != null : "fx:id=\"body\" was not injected: check your FXML file 'mainview.fxml'.";
		assert response != null : "fx:id=\"response\" was not injected: check your FXML file 'mainview.fxml'.";
		assert headerTable != null : "fx:id=\"headerTable\" was not injected: check your FXML file 'mainview.fxml'.";
		assert fileTable != null : "fx:id=\"headerTable\" was not injected: check your FXML file 'mainview.fxml'.";
		assert headerNameColumn != null : "fx:id=\"headerNameColumn\" was not injected: check your FXML file 'mainview.fxml'.";
		assert headerValueColumn != null : "fx:id=\"headerValueColumn\" was not injected: check your FXML file 'mainview.fxml'.";
		assert fileParamNameColumn != null : "fx:id=\"fileParamNameColumn\" was not injected: check your FXML file 'mainview.fxml'.";
		assert fileNameColumn != null : "fx:id=\"fileNameColumn\" was not injected: check your FXML file 'mainview.fxml'.";
		assert paramNameColumn != null : "fx:id=\"paramNameColumn\" was not injected: check your FXML file 'mainview.fxml'.";
		assert paramValueColumn != null : "fx:id=\"paramValueColumn\" was not injected: check your FXML file 'mainview.fxml'.";

		assert addHeaderButton != null : "fx:id=\"addHeaderButton\" was not injected: check your FXML file 'mainview.fxml'.";
		assert editHeaderButton != null : "fx:id=\"editHeaderButton\" was not injected: check your FXML file 'mainview.fxml'.";
		assert deleteHeaderButton != null : "fx:id=\"editHeaderButton\" was not injected: check your FXML file 'mainview.fxml'.";

		assert addFileButton != null : "fx:id=\"addFileButton\" was not injected: check your FXML file 'mainview.fxml'.";
		assert editFileButton != null : "fx:id=\"editFileButton\" was not injected: check your FXML file 'mainview.fxml'.";
		assert deleteFileButton != null : "fx:id=\"deleteFileButton\" was not injected: check your FXML file 'mainview.fxml'.";

		assert addParamButton != null : "fx:id=\"addParamButton\" was not injected: check your FXML file 'mainview.fxml'.";
		assert editParamButton != null : "fx:id=\"editParamButton\" was not injected: check your FXML file 'mainview.fxml'.";
		assert deleteParamButton != null : "fx:id=\"deleteparamButton\" was not injected: check your FXML file 'mainview.fxml'.";

		headerNameColumn.setCellValueFactory(cellData -> cellData.getValue().getHeaderNameProperty());
		headerNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		headerValueColumn.setCellValueFactory(cellData -> cellData.getValue().getHeaderValueProperty());
		headerValueColumn.setCellFactory(TextFieldTableCell.forTableColumn());

		headerTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection == null)
			{
				editHeaderButton.setDisable(true);
				deleteHeaderButton.setDisable(true);
			}
			else
			{
				editHeaderButton.setDisable(false);
				deleteHeaderButton.setDisable(false);
			}
		});

		editHeaderButton.setDisable(true);
		deleteHeaderButton.setDisable(true);

		fileParamNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFileParamNameProperty());
		fileParamNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		fileNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFileNameProperty());
		fileNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

		fileTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection == null)
			{
				editFileButton.setDisable(true);
				deleteFileButton.setDisable(true);
			}
			else
			{
				editFileButton.setDisable(false);
				deleteFileButton.setDisable(false);
			}
		});

		editFileButton.setDisable(true);
		deleteFileButton.setDisable(true);

		paramNameColumn.setCellValueFactory(cellData -> cellData.getValue().getParamNameProperty());
		paramNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		paramValueColumn.setCellValueFactory(cellData -> cellData.getValue().getParamValueProperty());
		paramValueColumn.setCellFactory(TextFieldTableCell.forTableColumn());

		paramTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection == null)
			{
				editParamButton.setDisable(true);
				deleteParamButton.setDisable(true);
			}
			else
			{
				editParamButton.setDisable(false);
				deleteParamButton.setDisable(false);
			}
		});

		editParamButton.setDisable(true);
		deleteParamButton.setDisable(true);

		initializeModel(null);
	}

	@FXML
	private void onPost(MouseEvent event)
	{
		if (!StringUtils.isEmpty(url.getText()))
		{
			IHttpService httpService = (url.getText().toLowerCase().startsWith("https:") ? new HttpsService() : new HttpService());
			try
			{
				Collection<RequestFile> files = model.getRequestFiles();
				Collection<RequestParam> params = model.getRequestParams();
				List<RequestHeader> headers = model.getRequestHeaders();
				String responseText;
				if (files.isEmpty() && params.isEmpty())
				{
					responseText = httpService.post(url.getText(), body.getText(), headers);
				}
				else
				{
					responseText = httpService.postForm(url.getText(), headers, files, params);
				}
				response.clear();
				response.appendText(responseText);
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				response.clear();
				response.appendText(e.getMessage());
			}
		}
	}

	@FXML
	private void onExit()
	{
		Platform.exit();
	}

	@FXML
	private void onSetConfDirectory()
	{
		ObjectMapper mapper = new ObjectMapper();

		File file = getFileWithConfDirectory();
		ConfigDirectory configDirectory = null;
		if (file.exists())
		{
			try
			{
				configDirectory = mapper.readValue(file, ConfigDirectory.class);
			}
			catch (Exception e)
			{
				logger.error("Failed reading ConfigDirectory. Error: " + e.getMessage(), e);
			}
		}

		DirectoryChooser dirChooser = new DirectoryChooser();
		dirChooser.setTitle("Choose folder with post configurations.");
		if (configDirectory != null)
		{
			dirChooser.setInitialDirectory(new File(configDirectory.getName()));
		}
		File directory = dirChooser.showDialog(getPrimaryStage());
		if (directory != null)
		{
			configDirectory = new ConfigDirectory(directory.getAbsolutePath());
			try
			{
				mapper.writeValue(file, configDirectory);
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				(new Alert(AlertType.ERROR, e.getMessage())).showAndWait();
			}
		}
	}

	public void onLoadRequest()
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choose file name to load request.");
		File confDirectory = getConfDirectory();
		if (confDirectory != null)
		{
			fileChooser.setInitialDirectory(confDirectory);
		}
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Request files (*.json)", "*.json"));

		File file = fileChooser.showOpenDialog(getPrimaryStage());
		if (file != null)
		{
			ObjectMapper mapper = new ObjectMapper();
			try
			{
				RequestConfigData requestConfigData = mapper.readValue(file, RequestConfigData.class);
				if (requestConfigData != null)
				{
					// url.setText(request.getUrlText());
					// body.setText(request.getBody());
					initializeModel(requestConfigData);
				}
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				(new Alert(AlertType.ERROR, e.getMessage())).showAndWait();
			}
		}

	}

	public void onSaveRequest()
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choose file name to save request.");
		File confDirectory = getConfDirectory();
		if (confDirectory != null)
		{
			fileChooser.setInitialDirectory(confDirectory);
		}
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Request files (*.json)", "*.json"));

		File file = fileChooser.showSaveDialog(getPrimaryStage());
		if (file != null)
		{
			PostRequestData postRequest = new PostRequestData(model.getPostRequest().getUrlText(), model.getPostRequest().getBody());

			List<RequestHeaderData> headers = new ArrayList<>();
			for (RequestHeader requestHeader : model.getRequestHeaders())
			{
				headers.add(new RequestHeaderData(requestHeader.getHeaderName(), requestHeader.getHeaderValue()));
			}

			List<RequestFileData> files = new ArrayList<>();
			for (RequestFile requestFile : model.getRequestFiles())
			{
				files.add(new RequestFileData(requestFile.getFileParamName(), requestFile.getFileName()));
			}

			List<RequestParamData> params = new ArrayList<>();
			for (RequestParam requestParam : model.getRequestParams())
			{
				params.add(new RequestParamData(requestParam.getParamName(), requestParam.getParamValue()));
			}

			RequestConfigData requestConfigData = new RequestConfigData(file.getName(), postRequest, headers, files, params);
			ObjectMapper mapper = new ObjectMapper();
			try
			{
				mapper.writeValue(file, requestConfigData);
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				(new Alert(AlertType.ERROR, e.getMessage())).showAndWait();
			}
		}
	}

	private Stage getPrimaryStage()
	{
		return (Stage) url.getScene().getWindow();
	}

	private File getFileWithConfDirectory()
	{
		String userDir = System.getProperty("user.dir");
		File file = new File(userDir, "configDirectory.json");
		return file;
	}

	private File getConfDirectory()
	{
		File fileWithConfig = getFileWithConfDirectory();
		if (fileWithConfig != null)
		{
			ObjectMapper mapper = new ObjectMapper();
			try
			{
				ConfigDirectory cd = mapper.readValue(fileWithConfig, ConfigDirectory.class);
				String fileName = cd.getName();
				if (!StringUtils.isEmpty(fileName))
				{
					File file = new File(fileName);
					return file;
				}
			}
			catch (IOException e)
			{
			}
		}
		return null;
	}

	private void initializeModel(final RequestConfigData requestConfigData)
	{

		if (requestConfigData == null)
		{
			model = new Model();
		}
		else
		{
			model = new Model();
			PostRequest postRequest = new PostRequest(requestConfigData.getPostRequest().getUrlText(), requestConfigData.getPostRequest().getBody());
			model.setPostRequest(postRequest);

			for (RequestHeaderData requestHeaderData : requestConfigData.getHeaders())
			{
				model.getRequestHeaders().add(new RequestHeader(requestHeaderData.getHeaderName(), requestHeaderData.getHeaderValue()));
			}

			for (RequestFileData requestFileData : requestConfigData.getFiles())
			{
				model.getRequestFiles().add(new RequestFile(requestFileData.getFileParamName(), requestFileData.getFileName()));
			}

			for (RequestParamData requestParamData : requestConfigData.getParams())
			{
				model.getRequestParams().add(new RequestParam(requestParamData.getParamName(), requestParamData.getParamValue()));
			}
		}

		url.textProperty().bindBidirectional(model.getPostRequest().getUrlTextProperty());
		body.textProperty().bindBidirectional(model.getPostRequest().getBodyProperty());
		headerTable.setItems(model.getRequestHeaders());
		fileTable.setItems(model.getRequestFiles());
		paramTable.setItems(model.getRequestParams());
	}

	public void onAddHeader()
	{
		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/headerupdate.fxml"));
			// VBox root =
			// FXMLLoader.load(getClass().getResource("/fxml/headerupdate.fxml"));
			VBox root = loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Add new header");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(getPrimaryStage());

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());

			dialogStage.setScene(scene);

			HeaderUpdateController headerUpdateController = loader.getController();
			RequestHeader requestHeader = new RequestHeader("", "");
			headerUpdateController.setDialogStage(dialogStage);
			headerUpdateController.setRequestHeader(requestHeader);

			dialogStage.showAndWait();

			if (headerUpdateController.isOkClicked())
			{
				model.getRequestHeaders().add(requestHeader);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void onEditHeader()
	{
		RequestHeader requestHeader = headerTable.getSelectionModel().getSelectedItem();

		if (requestHeader == null)
		{
			return;
		}

		RequestHeader requestHeaderSaved = new RequestHeader(requestHeader.getHeaderName(), requestHeader.getHeaderValue());

		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/headerupdate.fxml"));
			// VBox root =
			// FXMLLoader.load(getClass().getResource("/fxml/headerupdate.fxml"));
			VBox root = loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit header");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(getPrimaryStage());

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());

			dialogStage.setScene(scene);

			HeaderUpdateController headerUpdateController = loader.getController();
			headerUpdateController.setDialogStage(dialogStage);
			headerUpdateController.setRequestHeader(requestHeader);

			dialogStage.showAndWait();

			if (!headerUpdateController.isOkClicked())
			{
				headerTable.getSelectionModel().getSelectedItem().setHeaderName(requestHeaderSaved.getHeaderName());
				headerTable.getSelectionModel().getSelectedItem().setHeaderValue(requestHeaderSaved.getHeaderValue());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void onDeleteHeader()
	{
		int index = headerTable.getSelectionModel().getSelectedIndex();

		if (index < 0)
		{
			return;
		}

		Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure to delete this header?");
		alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> headerTable.getItems().remove(index));
	}

	public void onAddFile()
	{
		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/fileupdate.fxml"));
			VBox root = loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Add new file");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(getPrimaryStage());

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());

			dialogStage.setScene(scene);

			FileUpdateController fileUpdateController = loader.getController();
			RequestFile requestFile = new RequestFile("", "");
			fileUpdateController.setDialogStage(dialogStage);
			fileUpdateController.setRequestFile(requestFile);

			dialogStage.showAndWait();

			if (fileUpdateController.isOkClicked())
			{
				model.getRequestFiles().add(requestFile);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void onEditFile()
	{
		RequestFile requestFile = fileTable.getSelectionModel().getSelectedItem();

		if (requestFile == null)
		{
			return;
		}

		RequestFile requestFileSaved = new RequestFile(requestFile.getFileParamName(), requestFile.getFileName());

		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/fileupdate.fxml"));
			VBox root = loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit file");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(getPrimaryStage());

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());

			dialogStage.setScene(scene);

			FileUpdateController fileUpdateController = loader.getController();
			fileUpdateController.setDialogStage(dialogStage);
			fileUpdateController.setRequestFile(requestFile);

			dialogStage.showAndWait();

			if (!fileUpdateController.isOkClicked())
			{
				fileTable.getSelectionModel().getSelectedItem().setFileParamName(requestFileSaved.getFileParamName());
				fileTable.getSelectionModel().getSelectedItem().setFileName(requestFileSaved.getFileName());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void onDeleteFile()
	{
		int index = fileTable.getSelectionModel().getSelectedIndex();

		if (index < 0)
		{
			return;
		}

		Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure to delete this file?");
		alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> fileTable.getItems().remove(index));
	}

	public void onAddParam()
	{
		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/paramupdate.fxml"));
			VBox root = loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Add new param");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(getPrimaryStage());

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());

			dialogStage.setScene(scene);

			ParamUpdateController paramUpdateController = loader.getController();
			RequestParam requestParam = new RequestParam("", "");
			paramUpdateController.setDialogStage(dialogStage);
			paramUpdateController.setRequestParam(requestParam);

			dialogStage.showAndWait();

			if (paramUpdateController.isOkClicked())
			{
				model.getRequestParams().add(requestParam);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void onEditParam()
	{
		RequestParam requestParam = paramTable.getSelectionModel().getSelectedItem();

		if (requestParam == null)
		{
			return;
		}

		RequestParam requestParamSaved = new RequestParam(requestParam.getParamName(), requestParam.getParamValue());

		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/paramupdate.fxml"));
			VBox root = loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit param");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(getPrimaryStage());

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());

			dialogStage.setScene(scene);

			ParamUpdateController paramUpdateController = loader.getController();
			paramUpdateController.setDialogStage(dialogStage);
			paramUpdateController.setRequestParam(requestParam);

			dialogStage.showAndWait();

			if (!paramUpdateController.isOkClicked())
			{
				paramTable.getSelectionModel().getSelectedItem().setParamName(requestParamSaved.getParamName());
				paramTable.getSelectionModel().getSelectedItem().setParamValue(requestParamSaved.getParamValue());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void onDeleteParam()
	{
		int index = paramTable.getSelectionModel().getSelectedIndex();

		if (index < 0)
		{
			return;
		}

		Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure to delete this parameter?");
		alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> paramTable.getItems().remove(index));
	}

}
