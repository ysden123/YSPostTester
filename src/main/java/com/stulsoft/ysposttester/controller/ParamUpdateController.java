/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.controller;

import java.net.URL;
import java.util.ResourceBundle;
import com.stulsoft.ysposttester.model.RequestParam;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller for param update dialog.
 * 
 * @author Yuriy Stul
 *
 */
public class ParamUpdateController {
	private Stage dialogStage;
	private RequestParam requestParam;
	private boolean isOkClicked = false;

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="cancelButton"
	private Button cancelButton; // Value injected by FXMLLoader

	@FXML // fx:id="paramName"
	private TextField paramName; // Value injected by FXMLLoader

	@FXML // fx:id="paramValue"
	private TextField paramValue; // Value injected by FXMLLoader

	@FXML // fx:id="okButton"
	private Button okButton; // Value injected by FXMLLoader

	/**
	 * @return the dialogStage
	 */
	public Stage getDialogStage() {
		return dialogStage;
	}

	/**
	 * @param dialogStage
	 *            the dialogStage to set
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * @return the requestParam
	 */
	public RequestParam getRequestParam() {
		return requestParam;
	}

	/**
	 * @param requestParam
	 *            the requestParam to set
	 */
	public void setRequestParam(RequestParam requestParam) {
		this.requestParam = requestParam;

		paramName.textProperty().bindBidirectional(requestParam.getParamNameProperty());
		paramValue.textProperty().bindBidirectional(requestParam.getParamValueProperty());
	}

	/**
	 * @return the isOkClicked
	 */
	public boolean isOkClicked() {
		return isOkClicked;
	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		assert cancelButton != null : "fx:id=\"cancelButton\" was not injected: check your FXML file 'fileupdate.fxml'.";
		assert paramName != null : "fx:id=\"paramName\" was not injected: check your FXML file 'fileupdate.fxml'.";
		assert paramValue != null : "fx:id=\"paramValue\" was not injected: check your FXML file 'fileupdate.fxml'.";
		assert okButton != null : "fx:id=\"okButton\" was not injected: check your FXML file 'fileupdate.fxml'.";
	}

	@FXML
	public void onOkButton() {

		String errorMessage = "";
		if (requestParam.getParamName().isEmpty()) {
			errorMessage += " Param name is empty.";
		}

		if (errorMessage.isEmpty()) {
			isOkClicked = true;
			dialogStage.close();
		} else {
			Alert alert = new Alert(AlertType.ERROR, errorMessage);
			alert.showAndWait();
		}
	}

	@FXML
	public void onCancelButton() {
		isOkClicked = false;
		dialogStage.close();
	}
}
