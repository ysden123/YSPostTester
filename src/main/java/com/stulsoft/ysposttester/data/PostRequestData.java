/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.data;

/**
 * Contains data of POST request.
 * 
 * @author Yuriy Stul
 *
 */
public class PostRequestData {
	private String urlText;
	private String body;

	/**
	 * @return the urlText
	 */
	public String getUrlText() {
		return urlText;
	}

	/**
	 * @param urlText
	 *            the urlText to set
	 */
	public void setUrlText(String urlText) {
		this.urlText = urlText;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * Initializes a new instance of the PostRequestData class.
	 */
	protected PostRequestData() {
	}

	/**
	 * Initializes a new instance of the PostRequestData class.
	 * 
	 * @param urlText
	 * @param body
	 */
	public PostRequestData(String urlText, String body) {
		super();
		this.urlText = urlText;
		this.body = body;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + ((urlText == null) ? 0 : urlText.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostRequestData other = (PostRequestData) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (urlText == null) {
			if (other.urlText != null)
				return false;
		} else if (!urlText.equals(other.urlText))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostRequestData [urlText=" + urlText + ", body=" + body + "]";
	}

}
