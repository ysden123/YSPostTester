/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds configuration of a request.
 * 
 * @author Yuriy Stul
 *
 */
public class RequestConfigData {
	private String name;
	private PostRequestData postRequest;
	private List<RequestHeaderData> headers = new ArrayList<RequestHeaderData>();
	private List<RequestFileData> files = new ArrayList<RequestFileData>();
	private List<RequestParamData> params = new ArrayList<RequestParamData>();

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the postRequest
	 */
	public PostRequestData getPostRequest() {
		return postRequest;
	}

	/**
	 * @param postRequest
	 *            the postRequest to set
	 */
	public void setPostRequest(PostRequestData postRequest) {
		this.postRequest = postRequest;
	}

	/**
	 * @return the headers
	 */
	public List<RequestHeaderData> getHeaders() {
		return headers;
	}

	/**
	 * @param headers
	 *            the headers to set
	 */
	public void setHeaders(List<RequestHeaderData> headers) {
		this.headers = headers;
	}
	
	/**
	 * @return the files
	 */
	public List<RequestFileData> getFiles() {
		return files;
	}
	
	/**
	 * @param files
	 *            the files to set
	 */
	public void setFiles(List<RequestFileData> files) {
		this.files = files;
	}

	/**
	 * @return the params
	 */
	public List<RequestParamData> getParams()
	{
		return params;
	}

	/**
	 * @param params the params to set
	 */
	public void setParams(List<RequestParamData> params)
	{
		this.params = params;
	}

	/**
	 * Initializes a new instance of the RequestConfigData class.
	 */
	protected RequestConfigData() {
	}

	/**
	 * Initializes a new instance of the RequestConfigData class.
	 * 
	 * @param name
	 * @param postRequest
	 * @param headers
	 * @param params
	 */
	public RequestConfigData(final String name, final PostRequestData postRequest, final List<RequestHeaderData> headers, final List<RequestFileData> files, final List<RequestParamData> params) {
		super();
		this.name = name;
		this.postRequest = postRequest;
		this.headers = headers;
		this.files = files;
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((files == null) ? 0 : files.hashCode());
		result = prime * result + ((headers == null) ? 0 : headers.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((params == null) ? 0 : params.hashCode());
		result = prime * result + ((postRequest == null) ? 0 : postRequest.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestConfigData other = (RequestConfigData) obj;
		if (files == null)
		{
			if (other.files != null)
				return false;
		}
		else if (!files.equals(other.files))
			return false;
		if (headers == null)
		{
			if (other.headers != null)
				return false;
		}
		else if (!headers.equals(other.headers))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		if (params == null)
		{
			if (other.params != null)
				return false;
		}
		else if (!params.equals(other.params))
			return false;
		if (postRequest == null)
		{
			if (other.postRequest != null)
				return false;
		}
		else if (!postRequest.equals(other.postRequest))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "RequestConfigData [name=" + name + ", postRequest=" + postRequest + ", headers=" + headers + ", files=" + files + ", params=" + params + "]";
	}

}
