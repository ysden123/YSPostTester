/**
 * Copyright (c) 2016, William Hill Online. All rights reserved
 */
package com.stulsoft.ysposttester.data;

/**
 * Holds file details to send via post.
 * 
 * @author Yuriy Stul
 *
 */
public class RequestFileData
{
	private String fileParamName;
	private String fileName;

	/**
	 * Initializes a new instance of the RequestFileData class.
	 */
	protected RequestFileData(){
		
	}
	
	/**
	 * Initializes a new instance of the RequestFileData class.
	 * 
	 * @param fileParamName the file parameter name
	 * @param fileName the file ti upload.
	 */
	public RequestFileData(String fileParamName, String fileName)
	{
		super();
		this.fileParamName = fileParamName;
		this.fileName = fileName;
	}
	/**
	 * @return the fileParamName
	 */
	public String getFileParamName()
	{
		return fileParamName;
	}
	/**
	 * @param fileParamName the fileParamName to set
	 */
	public void setFileParamName(String fileParamName)
	{
		this.fileParamName = fileParamName;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName()
	{
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((fileParamName == null) ? 0 : fileParamName.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestFileData other = (RequestFileData) obj;
		if (fileName == null)
		{
			if (other.fileName != null)
				return false;
		}
		else if (!fileName.equals(other.fileName))
			return false;
		if (fileParamName == null)
		{
			if (other.fileParamName != null)
				return false;
		}
		else if (!fileParamName.equals(other.fileParamName))
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "RequestFile [fileParamName=" + fileParamName + ", fileName=" + fileName + "]";
	}

}
