/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.data;

/**
 * Holds a request header details.
 * 
 * @author Yuriy Stul
 *
 */
public class RequestHeaderData {
	private String headerName;
	private String headerValue;

	/**
	 * @return the headerName
	 */
	public String getHeaderName() {
		return headerName;
	}

	/**
	 * @param headerName
	 *            the headerName to set
	 */
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	/**
	 * @return the headerValue
	 */
	public String getHeaderValue() {
		return headerValue;
	}

	/**
	 * @param headerValue
	 *            the headerValue to set
	 */
	public void setHeaderValue(String headerValue) {
		this.headerValue = headerValue;
	}

	/**
	 * Initializes a new instance of the RequestHeaderData class. 
	 */
	protected RequestHeaderData(){}

	/**
	 * Initializes a new instance of the RequestHeaderData class.
	 * 
	 * @param headerName
	 * @param headerValue
	 */
	public RequestHeaderData(String headerName, String headerValue) {
		super();
		this.headerName = headerName;
		this.headerValue = headerValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((headerName == null) ? 0 : headerName.hashCode());
		result = prime * result + ((headerValue == null) ? 0 : headerValue.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestHeaderData other = (RequestHeaderData) obj;
		if (headerName == null) {
			if (other.headerName != null)
				return false;
		} else if (!headerName.equals(other.headerName))
			return false;
		if (headerValue == null) {
			if (other.headerValue != null)
				return false;
		} else if (!headerValue.equals(other.headerValue))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestHeaderData [headerName=" + headerName + ", headerValue=" + headerValue + "]";
	}
}
