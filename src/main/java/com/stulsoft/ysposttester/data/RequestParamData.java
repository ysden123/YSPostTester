/**
 * Copyright (c) 2016, William Hill Online. All rights reserved
 */
package com.stulsoft.ysposttester.data;

/**
 * Holds form parameters to send via post.
 * 
 * @author Yuriy Stul
 *
 */
public class RequestParamData
{
	private String paramName;
	private String paramValue;

	/**
	 * Initializes a new instance of the RequestParamData class.
	 */
	protected RequestParamData()
	{

	}

	/**
	 * Initializes a new instance of the RequestFileData class.
	 * 
	 * @param paramName
	 *            the parameter name
	 * @param paramValue
	 *            the parameter value.
	 */
	public RequestParamData(String paramName, String paramValue)
	{
		super();
		this.paramName = paramName;
		this.paramValue = paramValue;
	}

	/**
	 * @return the paramName
	 */
	public String getParamName()
	{
		return paramName;
	}

	/**
	 * @param fileParamName
	 *            the fileParamName to set
	 */
	public void setFileParamName(String fileParamName)
	{
		this.paramName = fileParamName;
	}

	/**
	 * @return the paramValue
	 */
	public String getParamValue()
	{
		return paramValue;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName)
	{
		this.paramValue = fileName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((paramName == null) ? 0 : paramName.hashCode());
		result = prime * result + ((paramValue == null) ? 0 : paramValue.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestParamData other = (RequestParamData) obj;
		if (paramName == null)
		{
			if (other.paramName != null)
				return false;
		}
		else if (!paramName.equals(other.paramName))
			return false;
		if (paramValue == null)
		{
			if (other.paramValue != null)
				return false;
		}
		else if (!paramValue.equals(other.paramValue))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "RequestParamData [paramName=" + paramName + ", paramValue=" + paramValue + "]";
	}

}
