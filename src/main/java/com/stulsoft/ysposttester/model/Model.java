/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.model;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Contains all data for UI.
 * 
 * @author Yuriy Stul
 *
 */
public class Model {
	private PostRequest postRequest = new PostRequest("", "");
	private ObservableList<RequestHeader> requestHeaders = FXCollections.observableArrayList(new ArrayList<RequestHeader>()); 
	private ObservableList<RequestFile> requestFiles = FXCollections.observableArrayList(new ArrayList<RequestFile>()); 
	private ObservableList<RequestParam> requestParams = FXCollections.observableArrayList(new ArrayList<RequestParam>()); 

	/**
	 * @return the postRequest
	 */
	public PostRequest getPostRequest() {
		return postRequest;
	}

	/**
	 * @param postRequest
	 *            the postRequest to set
	 */
	public void setPostRequest(PostRequest postRequest) {
		this.postRequest = postRequest;
	}

	/**
	 * @return the requestHeaders
	 */
	public ObservableList<RequestHeader> getRequestHeaders() {
		return requestHeaders;
	}

	/**
	 * @param requestHeaders
	 *            the requestHeaders to set
	 */
	public void setRequestHeaders(ObservableList<RequestHeader> requestHeaders) {
		this.requestHeaders = requestHeaders;
	}
	
	/**
	 * @return the requestFiles
	 */
	public ObservableList<RequestFile> getRequestFiles() {
		return requestFiles;
	}

	/**
	 * @param requestFiles
	 *            the requestHeaders to set
	 */
	public void setRequestFiles(ObservableList<RequestFile> requestFiles) {
		this.requestFiles = requestFiles;
	}
	
	/**
	 * @return the requestParams
	 */
	public ObservableList<RequestParam> getRequestParams() {
		return requestParams;
	}
	
	/**
	 * @param requestParams
	 *            the requestParams to set
	 */
	public void setRequestParams(ObservableList<RequestParam> requestParams) {
		this.requestParams = requestParams;
	}
}
