/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Contains details of the POST request.
 * 
 * @author Yuriy Stul
 *
 */
public class PostRequest {
	private SimpleStringProperty urlText = new SimpleStringProperty();
	private SimpleStringProperty body = new SimpleStringProperty();

	/**
	 * @return the urlText
	 */
	public String getUrlText() {
		return urlText.getValue();
	}
	
	/**
	 * Returns a urlTextProperty
	 * @return the urlTextProperty.
	 */
	public SimpleStringProperty getUrlTextProperty(){
		return urlText;
	}

	/**
	 * @param urlText
	 *            the urlText to set
	 */
	public void setUrlText(String urlText) {
		this.urlText.setValue(urlText);
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body.getValue();
	}
	
	/**
	 * Returns a bodyProperty
	 * @return the bodyProperty.
	 */
	public SimpleStringProperty getBodyProperty(){
		return body;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body.setValue(body);
	}

	/**
	 * Initializes a new instance of the PostRequest class.
	 * 
	 * @param urlText
	 *            specifies the server address.
	 * @param body
	 *            specifies the body content.
	 */
	public PostRequest(String urlText, String body) {
		super();
		setUrlText(urlText);
		setBody(body);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostRequest [urlText=" + urlText + ", body=" + body + "]";
	}
}
