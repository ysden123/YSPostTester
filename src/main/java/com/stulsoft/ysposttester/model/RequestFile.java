/**
 * Copyright (c) 2016, William Hill Online. All rights reserved
 */
package com.stulsoft.ysposttester.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Holds a file to upload details.
 * 
 * @author Yuriy Stul
 *
 */
public class RequestFile
{
	private StringProperty fileParamName = new SimpleStringProperty();
	private StringProperty fileName = new SimpleStringProperty();

	/**
	 * Initializes a new instance of the RequestFile
	 * 
	 * @param fileParamName
	 *            the file parameter name
	 * @param fileName
	 *            the file name
	 */
	public RequestFile(String fileParamName, String fileName)
	{
		setFileParamName(fileParamName);
		setFileName(fileName);
	}

	/**
	 * @return the fileParamName
	 */
	public String getFileParamName()
	{
		return fileParamName.getValue();
	}
	
	/**
	 * @return the fileParamName property
	 */
	public StringProperty getFileParamNameProperty()
	{
		return fileParamName;
	}

	/**
	 * @param fileParamName
	 *            the fileParamName to set
	 */
	public void setFileParamName(String fileParamName)
	{
		this.fileParamName.setValue(fileParamName);
	}

	/**
	 * @return the fileName
	 */
	public String getFileName()
	{
		return fileName.getValue();
	}
	
	/**
	 * @return the fileName property
	 */
	public StringProperty getFileNameProperty()
	{
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName)
	{
		this.fileName.setValue(fileName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "RequestFile [fileParamName=" + getFileParamName() + ", fileName=" + getFileName() + "]";
	}
}
