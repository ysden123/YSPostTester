/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Holds a request header details.
 * 
 * @author Yuriy Stul
 *
 */
public class RequestHeader {
	private StringProperty headerName = new SimpleStringProperty();
	private StringProperty headerValue = new SimpleStringProperty();

	/**
	 * @return the headerName
	 */
	public String getHeaderName() {
		return headerName.getValue();
	}

	/**
	 * Returns the headerName as SimpleStringProperty 
	 * @return the headerName as SimpleStringProperty
	 */
	public StringProperty getHeaderNameProperty(){
		return headerName;
	}
	/**
	 * @param headerName
	 *            the headerName to set
	 */
	public void setHeaderName(final String headerName) {
		this.headerName.setValue(headerName);
	}

	/**
	 * @return the headerValue
	 */
	public String getHeaderValue() {
		return headerValue.getValue();
	}

	/**
	 * Returns the headerValue as SimpleStringProperty 
	 * @return the headerValue as SimpleStringProperty
	 */
	public StringProperty getHeaderValueProperty(){
		return headerValue;
	}

	/**
	 * @param headerValue
	 *            the headerValue to set
	 */
	public void setHeaderValue(final String headerValue) {
		this.headerValue.setValue(headerValue);
	}

	/**
	 * @param headerName
	 * @param headerValue
	 */
	public RequestHeader(final String headerName, final String headerValue) {
		setHeaderName(headerName);
		setHeaderValue(headerValue);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestHeader [headerName=" + getHeaderName() + ", headerValue=" + getHeaderValue() + "]";
	}
}
