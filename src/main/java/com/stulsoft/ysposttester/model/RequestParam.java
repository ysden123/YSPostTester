/**
 * Copyright (c) 2016, William Hill Online. All rights reserved
 */
package com.stulsoft.ysposttester.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Holds a parameter details.
 * @author Yuriy Stul
 *
 */
public class RequestParam
{
	private StringProperty paramName = new SimpleStringProperty();
	private StringProperty paramValue = new SimpleStringProperty();

	/**
	 * Initializes a new instance of the RequestParam
	 * 
	 * @param paramName
	 *            the parameter name
	 * @param paramValue
	 *            the parameter value
	 */
	public RequestParam(String paramName, String paramValue)
	{
		setParamName(paramName);
		setParamValue(paramValue);
	}

	/**
	 * @return the paramName
	 */
	public String getParamName()
	{
		return paramName.getValue();
	}
	
	/**
	 * @return the paramName property
	 */
	public StringProperty getParamNameProperty()
	{
		return paramName;
	}

	/**
	 * @param paramName
	 *            the paramName to set
	 */
	public void setParamName(String paramName)
	{
		this.paramName.setValue(paramName);
	}

	/**
	 * @return the paramValue
	 */
	public String getParamValue()
	{
		return paramValue.getValue();
	}
	
	/**
	 * @return the paramValue property
	 */
	public StringProperty getParamValueProperty()
	{
		return paramValue;
	}

	/**
	 * @param paramValue
	 *            the paramValue to set
	 */
	public void setParamValue(String paramValue)
	{
		this.paramValue.setValue(paramValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "RequestFile [paramName=" + getParamName() + ", paramValue=" + getParamValue() + "]";
	}

}
