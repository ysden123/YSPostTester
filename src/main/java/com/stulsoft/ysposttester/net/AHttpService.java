/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.net;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.stulsoft.ysposttester.model.RequestFile;
import com.stulsoft.ysposttester.model.RequestHeader;
import com.stulsoft.ysposttester.model.RequestParam;

/**
 * Base class for IHttpService
 * 
 * @author Yuriy Stul
 *
 */
public abstract class AHttpService implements IHttpService
{
	private static Logger logger = LogManager.getLogger(AHttpService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.stulsoft.ysposttester.net.IHttpService#post(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String post(final String url, final String body, final Collection<RequestHeader> headers) throws ClientProtocolException, IOException
	{
		if (StringUtils.isEmpty(url))
		{
			return "URL is empty!";
		}

		logger.debug(getClass().getSimpleName() + " post to " + url);
		try (CloseableHttpClient client = createHttpClient())
		{
			HttpPost post = new HttpPost(url);

			// Adding headers
			if (headers != null)
			{
				headers.forEach(header -> post.addHeader(header.getHeaderName(), header.getHeaderValue()));
			}

			StringEntity bodyEntity = new StringEntity(body);
			post.setEntity(bodyEntity);
			logger.debug(getClass().getSimpleName() + " Sending request...");
			try (CloseableHttpResponse response = client.execute(post))
			{
				HttpEntity responseEntity = response.getEntity();
				String responseText = EntityUtils.toString(responseEntity);
				EntityUtils.consume(responseEntity);

				logger.debug(getClass().getSimpleName() + " response: " + responseText);
				return responseText;
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				return e.getClass().getName() + ": " + e.getMessage() + ". See log for details.";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.stulsoft.ysposttester.net.IHttpService#postForm(java.lang.String,
	 * java.util.Collection, java.util.Collection)
	 */
	@Override
	public String postForm(final String url, final Collection<RequestHeader> headers, final Collection<RequestFile> files, final Collection<RequestParam> params) throws ClientProtocolException, IOException
	{
		if (StringUtils.isEmpty(url))
		{
			return "URL is empty!";
		}

		logger.debug(getClass().getSimpleName() + " post to " + url);
		try (CloseableHttpClient client = createHttpClient())
		{
			HttpPost post = new HttpPost(url);

			// Adding headers
			if (headers != null)
			{
				headers.forEach(header -> post.addHeader(header.getHeaderName(), header.getHeaderValue()));
			}
			
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			// Adding parameters
			if (!params.isEmpty())
			{
				for(RequestParam param: params){
					builder.addTextBody(param.getParamName(), param.getParamValue());
				}
			}
			
			// Adding filess
			if (!files.isEmpty())
			{
				for(RequestFile file: files){
					File theFile = new File(file.getFileName());
					builder.addBinaryBody(file.getFileParamName(), theFile, ContentType.DEFAULT_BINARY, file.getFileName()); 
				}
			}
			
			HttpEntity entity = builder.build();
			post.setEntity(entity);
			
			logger.debug(getClass().getSimpleName() + " Sending request...");
			try (CloseableHttpResponse response = client.execute(post))
			{
				HttpEntity responseEntity = response.getEntity();
				String responseText = EntityUtils.toString(responseEntity);
				EntityUtils.consume(responseEntity);

				logger.debug(getClass().getSimpleName() + " response: " + responseText);
				return responseText;
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(), e);
				return e.getClass().getName() + ": " + e.getMessage() + ". See log for details.";
			}
		}
	}
}
