/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.net;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Yuriy Stul
 *
 */
public class HttpService extends AHttpService {
	private static Logger logger = LogManager.getLogger(HttpService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.stulsoft.ysposttester.net.IHttpService#createHttpClient()
	 */
	@Override
	public CloseableHttpClient createHttpClient() {
		logger.debug("Creating HTTP client.");
		CloseableHttpClient client = HttpClientBuilder.create().build();

		return client;
	}

}
