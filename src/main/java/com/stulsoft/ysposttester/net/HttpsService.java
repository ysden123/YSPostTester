/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.net;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Yuriy Stul
 *
 */
public class HttpsService extends AHttpService {
	private static Logger logger = LogManager.getLogger(HttpsService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.stulsoft.ysposttester.net.IHttpService#createHttpClient()
	 */
	@Override
	public CloseableHttpClient createHttpClient() {
		logger.debug("Creating HTTPS logger.");
		SSLContext sslContext = null;
		try {
			sslContext = SSLContexts.custom().loadTrustMaterial(new TrustStrategy() {
				public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					return true;
				}
			}).build();
		}
		catch (Exception e) {
			logger.error("Failed creating HTTPS client. Error: " + e.getMessage(), e);
			return null;
		}
		SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());

		HttpClientBuilder builder = HttpClientBuilder.create();

		builder.setSSLSocketFactory(sslSocketFactory);

		CloseableHttpClient client = builder.build();

		return client;
	}

}
