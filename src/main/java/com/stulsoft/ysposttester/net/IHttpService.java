/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.ysposttester.net;

import java.io.IOException;
import java.util.Collection;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.impl.client.CloseableHttpClient;
import com.stulsoft.ysposttester.model.RequestFile;
import com.stulsoft.ysposttester.model.RequestHeader;
import com.stulsoft.ysposttester.model.RequestParam;

/**
 * Defines the HTTP/HTTPS Service interface.
 * 
 * @author Yuriy Stul
 *
 */
public interface IHttpService
{
	/**
	 * Creates a HTTP/HTTPS client
	 * 
	 * @return the HTTP/HTTPS client.
	 */
	public CloseableHttpClient createHttpClient();

	/**
	 * Sends POST request and returns response.
	 * 
	 * @param url
	 *            server URL
	 * @param body
	 *            text to send as POST body
	 * @param headers
	 *            the headers
	 * @return server response.
	 * @throws ClientProtocolException
	 *             the protocol not supported
	 * @throws IOException
	 *             I/O error
	 */
	public String post(final String url, final String body, final Collection<RequestHeader> headers) throws ClientProtocolException, IOException;

	/**
	 * Sends form POST request and returns response.
	 * 
	 * @param url
	 *            server URL
	 * @param headers
	 *            the headers
	 * @param files
	 *            the files to upload
	 * @param params
	 *            the parameters
	 * @return server response.
	 * @throws ClientProtocolException
	 *             the protocol not supported
	 * @throws IOException
	 *             I/O error
	 */
	public String postForm(final String url, final Collection<RequestHeader> headers, final Collection<RequestFile> files, final Collection<RequestParam> params) throws ClientProtocolException, IOException;

}
