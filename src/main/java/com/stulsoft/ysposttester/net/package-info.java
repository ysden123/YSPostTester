/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */

/**
 * Net support.
 * 
 * @author Yuriy Stul
 *
 */
package com.stulsoft.ysposttester.net;