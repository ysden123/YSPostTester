/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */

/**
 * Root package for YSPostTester.
 * 
 * @author Yuriy Stul
 *
 */
package com.stulsoft.ysposttester;