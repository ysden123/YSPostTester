/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.test.ysposttester.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stulsoft.ysposttester.data.PostRequestData;

/**
 * @author Yuriy Stul
 *
 */
public class PostRequestDataTest {

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.PostRequestData#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		String urlText = "urlText";
		String body = "body";
		PostRequestData o1 = new PostRequestData(urlText, body);
		PostRequestData o2 = new PostRequestData(urlText, body);
		assertEquals(o1.hashCode(), o2.hashCode());
	}

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.PostRequestData#getUrlText()}.
	 */
	@Test
	public void testGetUrlText() {
		String urlText = "urlText";
		String body = "body";
		PostRequestData o1 = new PostRequestData(urlText, body);
		assertEquals(urlText, o1.getUrlText());
	}

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.PostRequestData#getBody()}.
	 */
	@Test
	public void testGetBody() {
		String urlText = "urlText";
		String body = "body";
		PostRequestData o1 = new PostRequestData(urlText, body);
		assertEquals(body, o1.getBody());
	}

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.PostRequestData#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		String urlText = "urlText";
		String body = "body";
		PostRequestData o1 = new PostRequestData(urlText, body);
		PostRequestData o2 = new PostRequestData(urlText, body);
		assertEquals(o1, o2);
	}
	
	/**
	 * Test for equal, hashCode, and Json conversion.
	 */
	@Test
	public void testJsonConversion(){
		String urlText = "urlText";
		String body = "body";
		PostRequestData postRequestOriginal = new PostRequestData(urlText, body);
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonText = mapper.writeValueAsString(postRequestOriginal);
			try {
				PostRequestData postRequetRead = mapper.readValue(jsonText, PostRequestData.class);
				assertEquals(postRequestOriginal, postRequetRead);
			}
			catch (IOException e) {
				fail(e.getMessage());
				e.printStackTrace();
			}
		}
		catch (JsonProcessingException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
	}
}
