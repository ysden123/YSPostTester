/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.test.ysposttester.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stulsoft.ysposttester.data.PostRequestData;
import com.stulsoft.ysposttester.data.RequestConfigData;
import com.stulsoft.ysposttester.data.RequestFileData;
import com.stulsoft.ysposttester.data.RequestHeaderData;
import com.stulsoft.ysposttester.data.RequestParamData;

/**
 * Unit tests for RequestConfigDataTest class.
 * 
 * @author Yuriy Stul
 *
 */
public class RequestConfigDataTest {

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.RequestConfigData#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		String name = "name";
		String urlText = "urlText";
		String body = "body";
		PostRequestData postRequestData = new PostRequestData(urlText, body);
		List<RequestHeaderData> headers = Arrays.asList(new RequestHeaderData("headerName", "headerValue"), new RequestHeaderData("headerName1", "headerValue1"));
		List<RequestFileData> files = Arrays.asList(new RequestFileData("p1",  "fileName1"), new RequestFileData("p2",  "fileName2"));
		List<RequestParamData> params = Arrays.asList(new RequestParamData("p1",  "v1"), new RequestParamData("p2",  "v2"));
		RequestConfigData requestConfigData1 = new RequestConfigData(name, postRequestData, headers, files, params);
		RequestConfigData requestConfigData2 = new RequestConfigData(name, postRequestData, headers, files, params);
		assertEquals(requestConfigData1.hashCode(), requestConfigData2.hashCode());
	}

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.RequestConfigData#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		String name = "name";
		String urlText = "urlText";
		String body = "body";
		PostRequestData postRequestData = new PostRequestData(urlText, body);
		List<RequestHeaderData> headers = Arrays.asList(new RequestHeaderData("headerName", "headerValue"), new RequestHeaderData("headerName1", "headerValue1"));
		List<RequestFileData> files = Arrays.asList(new RequestFileData("p1",  "fileName1"), new RequestFileData("p2",  "fileName2"));
		List<RequestParamData> params = Arrays.asList(new RequestParamData("p1",  "v1"), new RequestParamData("p2",  "v2"));
		RequestConfigData requestConfigData1 = new RequestConfigData(name, postRequestData, headers, files, params);
		RequestConfigData requestConfigData2 = new RequestConfigData(name, postRequestData, headers, files, params);
		assertEquals(requestConfigData1, requestConfigData2);
	}

	@Test
	public void testJSonConvert() {
		String name = "name";
		String urlText = "urlText";
		String body = "body";
		PostRequestData postRequestData = new PostRequestData(urlText, body);
		List<RequestHeaderData> headers = Arrays.asList(new RequestHeaderData("headerName", "headerValue"), new RequestHeaderData("headerName1", "headerValue1"));
		List<RequestFileData> files = Arrays.asList(new RequestFileData("p1",  "fileName1"), new RequestFileData("p2",  "fileName2"));
		List<RequestParamData> params = Arrays.asList(new RequestParamData("p1",  "v1"), new RequestParamData("p2",  "v2"));
		
		RequestConfigData requestConfigDataOriginal = new RequestConfigData(name, postRequestData, headers, files, params);
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonText = mapper.writeValueAsString(requestConfigDataOriginal);
			try {
				RequestConfigData requestConfigDataRead = mapper.readValue(jsonText, RequestConfigData.class);
				assertEquals(requestConfigDataOriginal, requestConfigDataRead);
			}
			catch (IOException e) {
				fail(e.getMessage());
				e.printStackTrace();
			}
		}
		catch (JsonProcessingException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
	}
}
