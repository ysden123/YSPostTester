/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.test.ysposttester.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stulsoft.ysposttester.data.RequestHeaderData;

/**
 * @author Yuriy Stul
 *
 */
public class RequestHeaderDataTest {

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.RequestHeaderData#getHeaderName()}.
	 */
	@Test
	public void testGetHeaderName() {
		String headerName = "headerName";
		String headerValue = "headerValue";
		assertEquals(headerName, (new RequestHeaderData(headerName, headerValue)).getHeaderName());
	}

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.RequestHeaderData#getHeaderValue()}.
	 */
	@Test
	public void testGetHeaderValue() {
		String headerName = "headerName";
		String headerValue = "headerValue";
		assertEquals(headerValue, (new RequestHeaderData(headerName, headerValue)).getHeaderValue());
	}

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.RequestHeaderData#RequestHeader(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRequestHeaderStringString() {
		String headerName = "headerName";
		String headerValue = "headerValue";
		RequestHeaderData header = new RequestHeaderData(headerName, headerValue);
		assertNotNull(header);
	}

	/**
	 * Test method for {@link com.stulsoft.ysposttester.data.RequestHeaderData#toString()}.
	 */
	@Test
	public void testToString() {
		String headerName = "headerName";
		String headerValue = "headerValue";
		String actual = (new RequestHeaderData(headerName, headerValue)).toString();
//		System.out.println(actual);
		String expected = "RequestHeaderData [headerName=headerName, headerValue=headerValue]";
		assertEquals(expected, actual);
	}

	/**
	 * Test for equal, hashCode, and Json conversion.
	 */
	@Test
	public void testJsonConversion(){
		String headerName = "headerName";
		String headerValue = "headerValue";
		RequestHeaderData headerOriginal = new RequestHeaderData(headerName, headerValue);
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonText = mapper.writeValueAsString(headerOriginal);
			try {
				RequestHeaderData headerRead = mapper.readValue(jsonText, RequestHeaderData.class);
				assertEquals(headerOriginal, headerRead);
			}
			catch (IOException e) {
				fail(e.getMessage());
				e.printStackTrace();
			}
		}
		catch (JsonProcessingException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
	}
}
