/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.test.ysposttester.net;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stulsoft.ysposttester.net.HttpService;

/**
 * Unit tests for {@link com.stulsoft.ysposttester.net.HttpService} and {@link com.stulsoft.ysposttester.net.AHttpService}
 * @author Yuriy Stul
 *
 */
public class HttpServiceTest {

	/**
	 * Test method for {@link com.stulsoft.ysposttester.net.AHttpService#post(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testPost() {
		String url = "http://httpbin.org/post";
		String body = "test";
		try {
			String response = new HttpService().post(url, body, null);
			// System.out.println("response text: " + response);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readTree(response);
			JsonNode dataNode = rootNode.path("data");
			assertNotNull(dataNode);
			String dataFromResponse= dataNode.textValue();
			assertNotNull(dataFromResponse);
			assertEquals(body, dataFromResponse);
		}
		catch (IOException e) {
			fail(e.getMessage());
		}
	}

}
