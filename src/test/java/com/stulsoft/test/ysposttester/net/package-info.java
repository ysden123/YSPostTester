/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */

/**
 * Unit tests for classes in com.stulsoft.ysposttester.net
 * 
 * @author Yuriy Stul
 *
 */
package com.stulsoft.test.ysposttester.net;