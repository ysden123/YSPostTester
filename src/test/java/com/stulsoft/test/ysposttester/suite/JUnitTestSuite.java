/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */
package com.stulsoft.test.ysposttester.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.stulsoft.test.ysposttester.data.PostRequestDataTest;
import com.stulsoft.test.ysposttester.data.RequestConfigDataTest;
import com.stulsoft.test.ysposttester.data.RequestHeaderDataTest;
import com.stulsoft.test.ysposttester.net.HttpServiceTest;
import com.stulsoft.test.ysposttester.net.HttpsServiceTest;

/**
 * @author Yuriy Stul
 *
 */
@RunWith(Suite.class)
//@formatter:off
@Suite.SuiteClasses({
	PostRequestDataTest.class,
	RequestHeaderDataTest.class,
	RequestConfigDataTest.class,
	HttpServiceTest.class,
	HttpsServiceTest.class
})
//@formatter:on
public class JUnitTestSuite {
}
