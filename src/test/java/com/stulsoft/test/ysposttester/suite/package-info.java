/**
 * Copyright (c) 2015, Yuriy Stul. All rights reserved
 */

/**
 * Specifies the test suites.
 * 
 * @author Yuriy Stul
 *
 */
package com.stulsoft.test.ysposttester.suite;